SET ANSI_WARNINGS ON;
GO

use master;
GO

IF EXISTS (SELECT name from master.dbo.sysdatabases WHERE name = N'mtd16')
DROP DATABASE mtd16;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'mtd16')
CREATE DATABASE mtd16;
GO

use mtd16;
GO

IF OBJECT_ID (N'dbo.patient', N'U') IS NOT NULL
DROP TABLE dbo.patient;
GO

CREATE TABLE dbo.patient
(
  pat_id SMALLINT not null identity(1,1),
  pat_ssn int NOT NULL check (pat_ssn > 0 and pat_ssn <= 999999999),
  pat_fname VARCHAR(15) NOT NULL,
  pat_lname VARCHAR(30) NOT NULL,
  pat_street VARCHAR(30) NOT NULL,
  pat_city VARCHAR(30) NOT NULL,
  pat_state CHAR(2) NOT NULL DEFAULT 'FL',
  pat_zip int NOT NULL check (pat_zip > 0 and pat_zip <= 999999999),
  pat_phone BIGINT NOT NULL check (pat_phone > 0 and pat_phone <= 9999999999),
  pat_email VARCHAR(100) NULL,
  pat_dob DATE NOT NULL,
  pat_gender CHAR(1) NOT NULL CHECK (pat_gender IN('m','f')),
  pat_notes VARCHAR(45) NULL,
  PRIMARY KEY (pat_id),

  CONSTRAINT ux_pat_ssn unique nonclustered (pat_ssn ASC)
);


-- Table dbo.medication
IF OBJECT_ID (N'dbo.medication', N'U') IS NOT NULL
DROP TABLE dbo.medication;

CREATE TABLE dbo.medication
(
  med_id SMALLINT NOT NULL identity(1,1),
  med_name VARCHAR(100) NOT NULL,
  med_price DECIMAL(5,2) NOT NULL CHECK (med_price > 0),
  med_shelf_life date NOT NULL,
  med_notes VARCHAR(255) NULL,
  PRIMARY KEY (med_id)
);

-- Table prescription
IF OBJECT_ID (N'dbo.prescription', N'U') IS NOT NULL
DROP TABLE dbo.prescription;

CREATE TABLE dbo.prescription
(
  pre_id SMALLINT NOT NULL identity(1,1),
  pat_id SMALLINT NOT NULL,
  med_id SMALLINT NOT NULL,
  pre_date DATE NOT NULL,
  pre_dosage VARCHAR(255) NOT NULL,
  pre_num_refills varchar(3) NOT NULL,
  pre_notes VARCHAR(255) NULL,
  PRIMARY KEY (pre_id),


CONSTRAINT ux_pat_id_med_id_pre_date unique nonclustered
(pat_id ASC, med_id ASC, pre_date ASC),

CONSTRAINT fk_prescription_patient
  FOREIGN KEY (pat_id)
  REFERENCES dbo.patient (pat_id)
  ON DELETE NO ACTION
  ON UPDATE CASCADE,

  CONSTRAINT fk_prescription_medication
    FOREIGN KEY (med_id)
    REFERENCES dbo.medication (med_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
);

--Table dbo.treatment

IF OBJECT_ID (N'dbo.treatment', N'U') IS NOT NULL
DROP TABLE dbo.treatment;

CREATE TABLE dbo.treatment
(
  trt_id SMALLINT NOT NULL identity(1,1),
  trt_name VARCHAR(255) NOT NULL,
  trt_price DECIMAL(8,2) NOT NULL CHECK (trt_price > 0),
  TRT_NOTES VARCHAR(255) NULL,
  PRIMARY KEY (trt_id)
);

--Table dbo.physician

IF OBJECT_ID (N'dbo.physician', N'U') IS NOT NULL
DROP TABLE dbo.physician;
GO

CREATE TABLE dbo.physician
(
  phy_id SMALLINT NOT NULL identity(1,1),
  phy_specialty VARCHAR(45) NOT NULL,
  phy_fname VARCHAR(15) NOT NULL,
  phy_lname VARCHAR(30) NOT NULL,
  phy_street VARCHAR(30) NOT NULL,
  phy_city VARCHAR(30) NOT NULL,
  phy_state CHAR(2) NOT NULL DEFAULT 'FL',
  phy_zip INT NOT NULL check (phy_zip > 0 and phy_zip <= 999999999),
  phy_phone BIGINT NOT NULL check (phy_phone > 0 and phy_phone <= 9999999999),
  phy_fax BIGINT NOT NULL check (phy_fax > 0 and phy_fax <= 9999999999),
  phy_email VARCHAR(100) NULL,
  phy_url VARCHAR(100) NULL,
  phy_notes VARCHAR(255) NULL,
  PRIMARY KEY (phy_id),
);

-- Table dbo.patient_treatment
IF OBJECT_ID (N'dbo.patient_treatment', N'U') IS NOT NULL
DROP TABLE dbo.patient_treatment;

CREATE TABLE dbo.patient_treatment
(
  ptr_id SMALLINT NOT NULL identity(1,1),
  pat_id SMALLINT NOT NULL,
  phy_id SMALLINT NOT NULL,
  trt_id SMALLINT NOT NULL,
  ptr_date DATE NOT NULL,
  ptr_start TIME(0) NOT NULL,
  ptr_end TIME(0) NOT NULL,
  ptr_results VARCHAR(255) NULL,
  ptr_notes VARCHAR(255) NULL,
  PRIMARY KEY (ptr_id),

  CONSTRAINT ux_pat_id_phy_id_trt_id_ptr_date unique nonclustered
  (pat_id ASC, phy_id ASC, trt_id ASC, ptr_date ASC),

  CONSTRAINT fk_patient_treatment_patient
    FOREIGN KEY (pat_id)
    REFERENCES dbo.patient (pat_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

  CONSTRAINT fk_patient_treatment_physician
    FOREIGN KEY (phy_id)
    REFERENCES dbo.physician (phy_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

  CONSTRAINT fk_patient_treatment_treatment
    FOREIGN KEY (trt_id)
    REFERENCES dbo.treatment (trt_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
);

-- Table dbo.administration_lu
IF OBJECT_ID (N'dbo.administration_lu', N'U') IS NOT NULL
DROP TABLE dbo.administration_lu;

CREATE TABLE dbo.administration_lu
(
	pre_id SMALLINT NOT NULL,
	ptr_id SMALLINT NOT NULL,
	PRIMARY KEY (pre_id, ptr_id),

	CONSTRAINT fk_administration_lu_prescription
		FOREIGN KEY (pre_id)
		REFERENCES dbo.prescription (pre_id)
		ON DELETE NO ACTION
		ON UPDATE CASCADE,

	CONSTRAINT fk_administration_lu_patient_treatment
		FOREIGN KEY (ptr_id)
		REFERENCES dbo.patient_treatment (ptr_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);
--show tables;
SELECT * FROM information_schema.tables;

EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"


-- data for table dbo.patient
INSERT INTO dbo.patient
(pat_ssn, pat_fname, pat_lname, pat_street, pat_city, pat_state, pat_zip, pat_phone, pat_email, pat_dob, pat_gender, pat_notes)

VALUES
('123456789', 'John', 'Lennon', '4637 Third Ave', 'New York', 'NY', '348574857', 6574938475, 'jlennon@gmail.com', '11-11-1941', 'M', NULL),
('475869584', 'Paul', 'McCartney', '5748 Broadway Avenue', 'Los Angeles', 'CA', '64857', 8602938475, 'paulmacca@yahoo.com', '4-04-1938', 'M', NULL),
('574857693', 'Ringo', 'Starr', '674 First Street', 'Chicago', 'IL', '57493', 8502938475, 'starrringo@comcast.net', '7-28-1943', 'M', NULL),
('384756203', 'George', 'Harrison', '3758 Eighth Ave', 'Philadalphia', 'PA', '57429', 5649783645, 'georgeharrison@yahoo.com', '4-08-1946', 'M', NULL),
('574089253', 'Brian', 'Epstein', '324 Swing Street', 'Brooklyn', 'NY', '57482', '1627869384', 'epsteinbrian@aol.com', '1-04-1935', 'M', NULL);


--Data for table dbo.medication
INSERT INTO dbo.medication
(med_name, med_price, med_shelf_life, med_notes)

VALUES
('Abilify', 200.00, '7-27-2013', NULL),
('Acophex', 125.00, '4-28-2012', NULL),
('Actonel', 320.00, '7-30-2013', NULL),
('Actos', 89.00, '06-27-2014', NULL),
('Adacdel', 66.00, '06-28-2014', NULL)

--Data for table dbo.prescription

INSERT INTO dbo.prescription
(pat_id, med_id, pre_date, pre_dosage, pre_num_refills, pre_notes)

VALUES
(1,1,'2011-12-23','take one per day', '1', NULL),
(1,2,'2011-12-24', 'take as needed', '2', NULL),
(1,3, '2011-12-25', 'take two before dinner', '1', NULL),
(1,4, '2011-12-26', 'take two in the morning', '2', NULL),
(1,5, '2011-12-27', 'take one before bed', '1', NULL)

-- data for table dbo.physician
INSERT INTO dbo.physician
(phy_specialty, phy_fname, phy_lname, phy_street, phy_city, phy_state, phy_zip, phy_phone, phy_fax, phy_email, phy_url, phy_notes)

VALUES
('family medicine', 'tom', 'smith', '987 Orange Ave', 'Orlando', 'FL', '33610', '47564', '8574639584', 'tsmith@gmail.com', 'tsmithfamilymed.com', NULL),
('pediatrician', 'ronald', 'burns', '645 Wave Circle', 'Tampa', 'FL', '33214', '9997465927', '7465740394', 'rburns@gmail.com', 'rburnspediatrics.com', NULL),
('psychiatrist', 'pete', 'roger', '1233 Stadium Lane', 'West Palm Beach', 'FL', '34991', '4758374658', '3948573645', 'proger@comcast.net', 'progerpsych.com', NULL),
('dermatology', 'dave', 'rogers', '654 Hard Drive', 'Miami', 'FL', '36631', '7485968473', '8448694637', 'daverogers@yahoo.com', 'rogersderma.com', NULL),
('cardiovascular surgery', 'ronald', 'mcdonald', '6758 Palm Tree Drive', 'Palm City', 'FL', '45869', '4755746374', '7584739485', 'ronmacdon@gmail.com', 'ronnymacscardio.com', NULL)

--Data for table dbo.treatment
INSERT INTO dbo.treatment
(trt_name, trt_price, trt_notes)

VALUES
('knee replacement', 2000.00, NULL),
('heart transplant', 130000.00, NULL),
('hip replacement', 40000.00, NULL),
('tonsils removed', 5000.00, NULL),
('skin graft', 2000.00, NULL)

--Data for table dbo.patient_treatment

INSERT INTO patient_treatment
(pat_id, phy_id, trt_id, ptr_date, ptr_start, ptr_end, ptr_results, ptr_notes)

VALUES
(1,5,1,'2011-12-23', '07:08:09', '10:12:15', 'sucess patient is fine', NULL),
(1,4,2,'2011-12-25', '08:08:09', '11:12:15', 'complications patient must return to complete procedure', NULL),
(2,3,3, '2011-12-26', '11:08:09', '14:12:15', 'success', NULL),
(2,2,4, '2011-12-27', '12:08:08', '14:12:15', 'died during surgery', NULL),
(3,1,5, '2011-12-28', '11:08:09', '12:12:12', 'success', NULL)

--Data for table dbo.administration_lu

INSERT INTO dbo.administration_lu
(pre_id, ptr_id)
VALUES (1,2), (3,4), (4,3), (5,3), (3,5)

exec sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"

--show Data
select * from dbo.patient;
select * from dbo.medication;
select * from dbo.prescription;
select* from dbo.physician;
select * from dbo.treatment;
select * from dbo.administration_lu;
