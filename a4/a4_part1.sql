use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'mtd16')
DROP DATABASE mtd16;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'mtd16')
CREATE DATABASE mtd16;
GO

use mtd16;
GO


IF OBJECT_ID (N'dbo.petstore', N'U') IS NOT NULL
DROP TABLE dbo.petstore;
GO

CREATE TABLE dbo.petstore
(
	pst_id TINYINT not null identity(1,1),
	pst_name VARCHAR(30) NOT NULL,
	pst_street VARCHAR(30) NOT NULL,
	pst_city VARCHAR(30) NOT NULL,
	pst_state CHAR(2) NOT NULL default 'AZ',
	pst_zip INT NOT NULL check (pst_zip > 0 and pst_zip <= 999999999),
	pst_phone BIGINT NOT NULL,
	pst_email VARCHAR(100) NOT NULL,
	pst_url VARCHAR(100) NOT NULL,
	pst_ytd_sales DECIMAL(10,2) NOT NULL check (pst_ytd_sales >- 0),
	pst_notes VARCHAR(255) NULL,
	primary key(pst_id)
);

SELECT * FROM INFORMATION_SCHEMA.TABLES;

insert into dbo.petstore
(pst_name,pst_street,pst_city,pst_state,pst_zip,pst_phone,pst_email,pst_url,pst_ytd_sales,pst_notes)
values
('Furry Friends','123 Main Street','New Orleans','LA',49580,485930675,'furryfriends@gmail.com','furryfriends.com',12500.00,'testing'),
('Perfect Pets','321 Broadway Avenue','Seattle','WA',79205,2839504697,'perfectpets@gmail.com','perfectpets.com',9500.00,'testing'),
('Happy Paws','987 First Avenue','New York','NY',23859,1528463957,'happypaws@gmail.com','happypaws.com',8700.00,'testing'),
('The Cats Meow','654 Seventh Avenue','Baton Rouge','LA',78493,2837465894,'catsmeow@gmail.com','catsmeow.com',17500.00,'testing'),
('Bingo Pets','753 Third Street','San Francisco','CA',12958,3940591746,'bingopets@gmail.com','bingopets.com',18000.00,'testing');


select * from dbo.petstore;


IF OBJECT_ID(N'dbo.pet', N'U') IS NOT NULL
DROP TABLE dbo.pet;
GO

CREATE TABLE dbo.pet
(
    pet_id SMALLINT NOT NULL identity(1,1),
    pst_id TINYINT NOT NULL,
    pet_type VARCHAR(45) NOT NULL,
    pet_sex CHAR(1) NOT NULL check (pet_sex IN('m','f')),
    pet_cost DECIMAL(6,2) NOT NULL check (pet_cost >- 0),
    pet_price DECIMAL(6,2) NOT NULL check (pet_price >- 0),
    pet_age SMALLINT NOT NULL check (pet_age > 0 and pet_age <= 10500),
    pet_color VARCHAR(30) NOT NULL,
    pet_sale_date DATE NULL,
    pet_vaccine CHAR(1) NOT NULL check (pet_vaccine IN('y','n')),
    pet_neuter CHAR(1) NOT NULL check (pet_neuter IN('y','n')),
    pet_notes VARCHAR(255) NULL,
    PRIMARY KEY (pet_id),
    CONSTRAINT fk_pet_petstore
    FOREIGN KEY (pst_id)
    REFERENCES dbo.petstore (pst_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

SELECT * FROM INFORMATION_SCHEMA.TABLES;

insert into dbo.pet
(pst_id,pet_type,pet_sex,pet_cost,pet_price,pet_age,pet_color,pet_sale_date,pet_vaccine,pet_neuter,pet_notes)
values
(1,'Dog','m',500.00,750.00,34,'brown',NULL,'y','y','testing'),
(2,'Cat','f',45.00,50.00,34,'white',NULL,'y','n','testing'),
(3,'Frog','f',10.00,20.00,34,'green',NULL,'n','n','testing'),
(4,'Dog','m',25.00,30.00,34,'white',NULL,'y','n','testing'),
(5,'Mouse','m',12.00,20.00,34,'grey',NULL,'n','n','testing');

select * from dbo.pet;


EXEC sp_help 'dbo.pet';




