--A5 MS SQL Server
--show warnings
SET ANSI_WARNINGS ON;
GO

use master;
GO

--drop existing db if exists
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'mtd16')
DROP DATABASE mtd16;
GO

--create db if not exists
IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'mtd16')
CREATE DATABASE mtd16;
GO

use mtd16;
GO

-- Table dbo.applicant
IF OBJECT_ID (N'dbo.applicant', N'U') IS NOT NULL
DROP TABLE dbo.applicant;
GO

CREATE TABLE dbo.applicant
(
  app_id SMALLINT not null identity(1,1),
  app_ssn int NOT NULL check (app_ssn > 0 and app_ssn <= 999999999),
  app_state_id VARCHAR(45) NOT NULL,
  app_fname VARCHAR(15) NOT NULL,
  app_lname VARCHAR(30) NOT NULL,
  app_street VARCHAR(30) NOT NULL,
  app_city VARCHAR(30) NOT NULL,
  app_state CHAR(2) NOT NULL DEFAULT 'FL',
  app_zip int NOT NULL check (app_zip > 0 AND app_zip <= 999999999),
  app_email VARCHAR(100) NULL,
  app_dob DATE NOT NULL,
  app_gender CHAR(1) NOT NULL CHECK (app_gender IN('m', 'f')),
  app_bckgd_check CHAR(1) NOT NULL CHECK (app_bckgd_check IN('n', 'y')),
  app_notes VARCHAR(45) NULL,
  PRIMARY KEY (app_id),

  --make sure SSNs and State IDS are unique
  CONSTRAINT ux_app_ssn unique nonclustered (app_ssn ASC),
  CONSTRAINT ux_app_state_id unique nonclustered (app_state_id ASC)
);

--Table property
IF OBJECT_ID (N'dbo.property', N'U') IS NOT NULL
DROP TABLE dbo.property;

CREATE TABLE dbo.property
(
  prp_id SMALLINT NOT NULL identity(1,1),
  prp_street VARCHAR(30) NOT NULL,
  prp_city VARCHAR(30) NOT NULL,
  prp_state CHAR(2) NOT NULL DEFAULT 'FL',
  prp_zip INT NOT NULL check (prp_zip > 0 and prp_zip <= 999999999),
  prp_type VARCHAR(15) NOT NULL CHECK
    (prp_type IN('house', 'condo', 'townhouse', 'duplex', 'apt', 'mobile home', 'room')),
  prp_rental_rate DECIMAL(7,2) NOT NULL CHECK (prp_rental_rate > 0),
  prp_status CHAR(1) NOT NULL CHECK (prp_status IN('a','u')),
  prp_notes VARCHAR(255) NULL,
  PRIMARY KEY (prp_id)
);

--Table dbo.agreement

IF OBJECT_ID (N'dbo.agreement', N'U') IS NOT NULL
DROP TABLE dbo.agreement;

CREATE TABLE dbo.agreement
(
  agr_id SMALLINT NOT NULL identity(1,1),
  prp_id SMALLINT NOT NULL,
  app_id SMALLINT NOT NULL,
  agr_signed DATE NOT NULL,
  agr_start DATE NOT NULL,
  agr_end DATE NOT NULL,
  agr_amt DECIMAL(7,2) NOT NULL CHECK (agr_amt > 0),
  agr_notes VARCHAR(255) NULL,
  PRIMARY KEY (agr_id),

  CONSTRAINT ux_prp_id_app_id_agr_signed unique nonclustered
    (prp_id ASC, app_id ASC, agr_signed ASC),

  CONSTRAINT fk_agreement_property
    FOREIGN KEY (prp_id)
    REFERENCES dbo.property (prp_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

  CONSTRAINT fk_agreement_applicant
    FOREIGN KEY (app_id)
    REFERENCES dbo.applicant (app_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

--Table dbo.feature
IF OBJECT_ID (N'dbo.feature', N'U') IS NOT NULL
DROP TABLE dbo.feature;

CREATE TABLE dbo.feature
(
  ftr_id TINYINT NOT NULL identity(1,1),
  ftr_type VARCHAR(45) NOT NULL,
  ftr_notes VARCHAR(255) NULL,
  PRIMARY KEY (ftr_id)
);

--Table dbo.prop_feature

IF OBJECT_ID (N'dbo.prop_feature', N'U') IS NOT NULL
DROP TABLE dbo.prop_feature;

CREATE TABLE dbo.prop_feature
(
  pft_id SMALLINT NOT NULL identity(1,1),
  prp_id SMALLINT NOT NULL,
  ftr_id TINYINT NOT NULL,
  pft_notes VARCHAR(255) NULL,
  PRIMARY KEY (pft_id),

  CONSTRAINT ux_prp_id_ftr_id unique nonclustered (prp_id ASC, ftr_id ASC),

  CONSTRAINT fk_prop_feat_property
    FOREIGN KEY (prp_id)
    REFERENCES dbo.property (prp_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

  CONSTRAINT fk_prop_feat_feature
    FOREIGN KEY (ftr_id)
    REFERENCES dbo.feature (ftr_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.occupant', N'U') IS NOT NULL
DROP TABLE dbo.occupant;

CREATE TABLE dbo.occupant
(
  ocp_id SMALLINT NOT NULL identity(1,1),
  app_id SMALLINT NOT NULL,
  ocp_ssn int NOT NULL check (ocp_ssn > 0 and ocp_ssn <=999999999),
  ocp_state_id VARCHAR(45) NULL,
  ocp_fname VARCHAR(15) NOT NULL,
  ocp_lname VARCHAR(30) NOT NULL,
  ocp_email VARCHAR(100) NULL,
  ocp_dob DATE NOT NULL,
  ocp_gender CHAR(1) NOT NULL CHECK (ocp_gender IN('m', 'f')),
  ocp_bckgd_check CHAR(1) NULL CHECK (ocp_bckgd_check IN('n', 'y')),
  ocp_notes VARCHAR(45) NULL,
  PRIMARY KEY (ocp_id),

  --make sure SSNs and State IDs are unique
  CONSTRAINT ux_ocp_ssn unique nonclustered (ocp_ssn ASC),
  CONSTRAINT ux_ocp_state_id unique nonclustered (ocp_state_id ASC),

  CONSTRAINT fk_occupant_applicant
    FOREIGN KEY (app_id)
    REFERENCES dbo.applicant (app_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- Table dbo.phone

IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;

CREATE TABLE dbo.phone
(
  phn_id SMALLINT NOT NULL identity(1,1),
  app_id SMALLINT NOT NULL,
  ocp_id SMALLINT NULL,
  phn_num bigint NOT NULL check (phn_num > 0 and phn_num <= 9999999999),
  phn_type CHAR(1) NOT NULL CHECK (phn_type IN('c','h','w','f')),
  phn_notes VARCHAR(45) NULL,
  PRIMARY KEY (phn_id),

  --make sure combination of app_id and phn_num is unique
  CONSTRAINT ux_app_id_phn_num unique nonclustered (app_id ASC, phn_num ASC),

  --make sure combination of ocp_id and phn_num is unique
  CONSTRAINT ux_ocp_id_phn_num unique nonclustered (ocp_id ASC, phn_num ASC),

  CONSTRAINT fk_phone_applicant
    FOREIGN KEY (app_id)
    REFERENCES dbo.applicant (app_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

  CONSTRAINT fk_phone_occupant
    FOREIGN KEY (ocp_id)
    REFERENCES dbo.occupant (ocp_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

--Table dbo.room_type

IF OBJECT_ID (N'dbo.room_type', N'U') IS NOT NULL
DROP TABLE dbo.room_type;

CREATE TABLE dbo.room_type
(
  rtp_id TINYINT NOT NULL identity(1,1),
  rtp_name VARCHAR(45) NOT NULL,
  rtp_notes VARCHAR(45) NULL,
  PRIMARY KEY (rtp_id)
);

--Table dbo.room
IF OBJECT_ID (N'dbo.room', N'U') IS NOT NULL
DROP TABLE dbo.room;

CREATE TABLE dbo.room
(
  rom_id SMALLINT NOT NULL identity(1,1),
  prp_id SMALLINT NOT NULL,
  rtp_id TINYINT NOT NULL,
  rom_size VARCHAR(45) NOT NULL,
  rom_notes VARCHAR(255) NULL,
  PRIMARY KEY (rom_id),

  CONSTRAINT fk_room_property
    FOREIGN KEY (prp_id)
    REFERENCES dbo.property (prp_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

  CONSTRAINT fk_room_roomtype
    FOREIGN KEY (rtp_id)
    REFERENCES dbo.room_type (rtp_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

--show tables;
SELECT * FROM information_schema.tables;

--disable all constraints (must do this *after* table creation, but  *before* inserts)
EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

--Data for table dbo.feature
INSERT INTO dbo.feature
(ftr_type, ftr_notes)

VALUES
('Central A/C', NULL),
('Pool', NULL),
('Close to school', NULL),
('Furnished', NULL),
('Cable', NULL),
('Washer/Dryer', NULL),
('Refrigerator', NULL),
('Microwave', NULL),
('Oven', NULL),
('1-car garage', NULL),
('2-car garage', NULL),
('Sprinkler system', NULL),
('Security', NULL),
('Wi-Fi', NULL),
('Storage', NULL),
('Fireplace', NULL);

--data for table dbo.room_type

INSERT INTO dbo.room_type
(rtp_name, rtp_notes)

VALUES
('Bed', NULL),
('Bath', NULL),
('Kitchen', NULL),
('Lanai', NULL),
('Dining', NULL),
('Living', NULL),
('Basement', NULL),
('Office', NULL);

--Data for table dbo.prop_feature
INSERT INTO dbo.prop_feature
(prp_id, ftr_id, pft_notes)

VALUES
(1, 4, NULL),
(2, 5, NULL),
(3, 3, NULL),
(4, 2, NULL),
(5, 1, NULL),
(1, 1, NULL),
(1, 5, NULL);

--Data for table dbo.room
INSERT INTO dbo.room
(prp_id, rtp_id, rom_size, rom_notes)

VALUES
(1,1,'10'' x 10''', NULL),
(3,2,'20'' x 15''', NULL),
(4,3,'8'' x 8''', NULL),
(5,4,'50'' x 50''', NULL),
(2,5,'30'' x 30''', NULL);

--Data for table dbo.property
INSERT INTO dbo.property
(prp_street, prp_city, prp_state, prp_zip, prp_type, prp_rental_rate, prp_status, prp_notes)

VALUES
('74 Third Street', 'Palm Beach', 'FL', '948857493', 'apt', 1700.00, 'u', NULL),
('837 Ocean Avenue', 'Jensen Beach', 'FL', '384750293', 'house', 440.00, 'u', NULL),
('475 Broadway Avenue', 'San Francisco', 'CA', '574639586', 'townhouse', 3400.00, 'a', NULL),
('574 Doritos Circle', 'Jacksonville', 'FL', '365231234', 'condo', 2100.00, 'a', NULL),
('1 Innovation Drive', 'Sillicon Valley', 'CA', '584738675', 'apt', 720.00, 'u', NULL);

--Data for table applicant
INSERT INTO dbo.applicant
(app_ssn, app_state_id, app_fname, app_lname, app_street, app_city, app_state, app_zip, app_email, app_dob, app_gender, app_bckgd_check, app_notes)

VALUES
('123456789', 'A12C3456D789', 'Billy', 'Joel', '7738 Piano Road', 'Long Island City', 'NY', '334671234', 'joelbilly@yahoo.com', '1961-10-23', 'M', 'y', NULL),
('590123654', 'B123A456D789', 'Martha', 'Stewart', '1134 Mountain Valley Road', 'Napa Valey', 'CA', '675840293', 'mstu@my.fsu.edu', '1961-04-04', 'F', 'y', NULL),
('987456321', 'dfed66532sedd', 'Joe', 'Pesci', '4738 Coconut Drive', 'Honolulu', 'HI', '323081234', 'joepesh@comcast.net', '1955-05-15', 'M', 'n', NULL),
('365214986', 'dgfgr56597224', 'George', 'Harrison', '987 Liverpool Way', 'Los Angeles', 'CA', '323011234', 'georgeh@fsu.edu', '1941-07-25', 'M', 'y', NULL),
('326598236', 'yadayada4517', 'Stevie', 'Nicks', '787 Rumors Road', 'Bethesda', 'MD', '323061234', 'nicksstevie@my.fsu.edu', '1950-08-14', 'M', 'n', NULL);

--data for table dbo.agreement
INSERT INTO dbo.agreement
(prp_id, app_id, agr_signed, agr_start, agr_end, agr_amt, agr_notes)

VALUES
(3, 4, '2003-10-01', '2012-01-01', '2012-12-31', 1050.00, NULL),
(1, 1, '1993-03-01', '1983-01-01', '1987-12-31', 850.00, NULL),
(4, 2, '1989-12-31', '2000-01-01', '2004-12-31', 1300.00, NULL),
(5, 3, '1994-07-31', '1999-08-01', '2004-07-31', 740.00, NULL),
(2, 5, '2001-01-01', '2011-01-01', '2013-12-31', 950.00, NULL);

--Data for table dbo.occupant

INSERT INTO dbo.occupant
(app_id, ocp_ssn, ocp_state_id, ocp_fname, ocp_lname, ocp_email, ocp_dob, ocp_gender, ocp_bckgd_check, ocp_notes)

VALUES
(1, '645364857', 'of4857483', 'Bridget', 'Jones', 'bsj10c@gmail.com', '1978-03-13', 'F', 'y', NULL),
(1, '394850394', 'kgh57483', 'Brian', 'Williams', 'brian@nbc.com', '1956-07-28', 'M', 'y', NULL),
(2, '837840293', 'ghg4848555', 'John', 'Lennon', 'hippiejohn@beatles.com', '2009-01-01', 'F', 'n', NULL),
(2, '039485730', 'ghf384857', 'Biggie', 'Smalls', 'big@badboyent.com', '1968-03-05', 'M', 'n', NULL),
(5, '029383746', 'lal3847566', 'Shawn', 'Carter', 'jayz@defjam.com', '1974-04-08', 'F', 'n', NULL);

--Data for table dbo.phone
INSERT INTO dbo.phone
(app_id, ocp_id, phn_num, phn_type, phn_notes)

VALUES
(1, NULL, '8609374869', 'H', NULL),
(2, NULL, '8503948475', 'C', NULL),
(5, 5, '6758493847', 'H', NULL),
(1, 1, '675938475', 'F', NULL),
(3, NULL, '987304958', 'W', NULL);

--enable all constraints
exec sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"

select * from dbo.feature;
select * from dbo.prop_feature;
select * from dbo.room_type;
select * from dbo.room;
select * from dbo.property;
select * from dbo.applicant;
select * from dbo.agreement;
select * from dbo.occupant;
select * from dbo.phone;

-- part 2

--a

USE mtd16;
GO

begin transaction;
SELECT * from dbo.applicant;
SELECT * from dbo.occupant;
SELECT * from dbo.phone;
SELECT * from dbo.agreement;

DELETE FROM dbo.applicant
WHERE app_id = 1;

SELECT * from dbo.applicant;
SELECT * from dbo.occupant;
SELECT * from dbo.phone;
SELECT * from dbo.agreement;
commit;

--b

use mtd16;
go

IF OBJECT_ID (N'dbo.v_prop_info',N'V') IS NOT NULL
DROP VIEW dbo.v_prop_info;
GO

CREATE VIEW dbo.v_prop_info as
select p.prp_id, prp_type, prp_rental_rate, rtp_name, rom_size
from property p
JOIN room r on p.prp_id = r.prp_id
JOIN room_type rt on r.rtp_id=rt.rtp_id
where p.prp_id=3;

GO

SELECT * from information_schema.tables;
go

--c

use mtd16;
go

IF OBJECT_ID (N'dbo.v_prop_info_feature',N'V') IS NOT NULL
DROP VIEW dbo.v_prop_info_feature;
GO

create view dbo.v_prop_info_feature as
select TOP 99 PERCENT p.prp_id, prp_type, prp_rental_rate, ftr_type
from property p
JOIN prop_feature pf on p.prp_id=pf.prp_id
JOIN feature f on pf.ftr_id=f.ftr_id
where p.prp_id >= 4 and p.prp_id < 6;
go

select * from dbo.v_prop_info_feature order by prp_rental_rate desc;
go

SELECT * FROM information_schema.tables;
go

--d

USE mtd16;
go

IF OBJECT_ID ('dbo.applicantinfo') IS NOT NULL
DROP PROCEDURE dbo.applicantinfo;
GO

CREATE PROCEDURE dbo.applicantinfo(@appid INT) AS
select app_ssn, app_state_id, app_fname, app_lname, phn_num, phn_type
from applicant a, phone p
where a.app_id = p.app_id
and a.app_id=@appid;
GO

EXEC dbo.applicantinfo 2;

--e

use mtd16;
go

IF OBJECT_ID ('dbo.OccupantInfo') IS NOT NULL
DROP PROCEDURE dbo.OccupantInfo;
GO

CREATE PROCEDURE dbo.OccupantInfo AS
SELECT ocp_ssn, ocp_state_id, ocp_fname, ocp_lname, phn_num, phn_type
from phone p
	LEFT OUTER JOIN occupant o ON o.ocp_id = p.ocp_id;
GO

EXEC dbo.OccupantInfo;
GO

DROP PROCEDURE dbo.OccupantInfo;
GO
