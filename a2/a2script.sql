-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mtd16
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mtd16` ;

-- -----------------------------------------------------
-- Schema mtd16
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mtd16` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `mtd16` ;

-- -----------------------------------------------------
-- Table `mtd16`.`person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mtd16`.`person` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mtd16`.`person` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_ssn` INT UNSIGNED NOT NULL,
  `per_fname` VARCHAR(15) NOT NULL,
  `per_lname` VARCHAR(30) NOT NULL,
  `per_gender` ENUM('m', 'f') NOT NULL,
  `per_dob` DATE NOT NULL,
  `per_street` VARCHAR(45) NOT NULL,
  `per_city` VARCHAR(45) NOT NULL,
  `per_state` CHAR(2) NOT NULL,
  `per_zip` INT UNSIGNED NOT NULL,
  `per_phone` BIGINT UNSIGNED NOT NULL,
  `per_email` VARCHAR(100) NOT NULL,
  `per_is_emp` ENUM('y', 'n') NOT NULL,
  `per_is_alm` ENUM('y', 'n') NOT NULL,
  `per_is_stu` ENUM('y', 'n') NOT NULL,
  `per_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mtd16`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mtd16`.`employee` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mtd16`.`employee` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL,
  `emp_title` VARCHAR(20) NOT NULL,
  `emp_salary` DECIMAL(8,2) NOT NULL,
  `emp_is_fac` ENUM('y', 'n') NOT NULL,
  `emp_is_stf` ENUM('y', 'n') NOT NULL,
  `emp_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_employee_person`
    FOREIGN KEY (`per_id`)
    REFERENCES `mtd16`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mtd16`.`faculty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mtd16`.`faculty` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mtd16`.`faculty` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL,
  `fac_rank` VARCHAR(45) NOT NULL,
  `fac_start_date` VARCHAR(45) NOT NULL,
  `fac_end_date` VARCHAR(45) NULL,
  `fac_notes` VARCHAR(45) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_faculty_employee1`
    FOREIGN KEY (`per_id`)
    REFERENCES `mtd16`.`employee` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mtd16`.`student`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mtd16`.`student` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mtd16`.`student` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL,
  `stu_major` VARCHAR(30) NOT NULL,
  `stu_is_ugd` ENUM('y', 'n') NOT NULL,
  `stu_is_grd` ENUM('y', 'n') NOT NULL,
  `stu_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_student_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `mtd16`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mtd16`.`undergrad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mtd16`.`undergrad` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mtd16`.`undergrad` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL,
  `ugd_test` ENUM('sat', 'act') NOT NULL,
  `ugd_score` SMALLINT UNSIGNED NOT NULL,
  `ugd_standing` ENUM('fr', 'so', 'jr', 'sr') NOT NULL,
  `ugd_start_date` DATE NOT NULL,
  `ugd_end_date` DATE NULL,
  `ugd_notes` VARCHAR(255) NULL COMMENT 'SAT score range of 600-2400.\nAct main four tests scored individually on a scale of 1-36, a Composite score is provided which is average of the four scores.',
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_undergrad_student1`
    FOREIGN KEY (`per_id`)
    REFERENCES `mtd16`.`student` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mtd16`.`grad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mtd16`.`grad` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mtd16`.`grad` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL,
  `grd_test` ENUM('gre', 'gmat', 'lsat') NOT NULL,
  `grd_score` SMALLINT UNSIGNED NOT NULL,
  `grd_start_date` DATE NOT NULL,
  `grd_end_date` DATE NULL,
  `grd_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_grad_student1`
    FOREIGN KEY (`per_id`)
    REFERENCES `mtd16`.`student` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mtd16`.`staff`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mtd16`.`staff` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mtd16`.`staff` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL,
  `stf_position` VARCHAR(30) NOT NULL,
  `stf_start_date` DATE NOT NULL,
  `stf_end_date` DATE NULL,
  `stf_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_staff_employee1`
    FOREIGN KEY (`per_id`)
    REFERENCES `mtd16`.`employee` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mtd16`.`alumnus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mtd16`.`alumnus` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mtd16`.`alumnus` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL,
  `alm_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_alumnus_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `mtd16`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mtd16`.`degree`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mtd16`.`degree` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mtd16`.`degree` (
  `deg_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_id` MEDIUMINT UNSIGNED NOT NULL,
  `deg_type` VARCHAR(30) NOT NULL,
  `deg_area` VARCHAR(30) NOT NULL,
  `deg_date` DATE NOT NULL,
  `deg_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`deg_id`),
  INDEX `fk_degree_alumnus1_idx` (`per_id` ASC),
  CONSTRAINT `fk_degree_alumnus1`
    FOREIGN KEY (`per_id`)
    REFERENCES `mtd16`.`alumnus` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mtd16`.`person`
-- -----------------------------------------------------
START TRANSACTION;
USE `mtd16`;
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (1, 893961457, 'Ralph', 'Flowers', 'm', '2014-06-23', 'Ap #891-1904 Risus. Av.', 'Provo', 'UT', 829453726, 2245393138, 'dapibus.id.blandit@ipsumcursus.org', 'n', 'n', 'y', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (2, 290623966, 'Phelan', 'Byrd', 'm', '2008-05-13', 'Ap #931-6225 Molestie St.', 'Sandy', 'UT', 195307934, 6145666386, 'Cum.sociis.natoque@malesuadaid.edu', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (3, 538859505, 'Rashad', 'Delaney', 'f', '1988-06-02', 'P.O. Box 945, 1377 Fringilla Road', 'New Haven', 'CT', 242534897, 9357958179, 'imperdiet.nec@pedePraesenteu.org', 'y', 'n', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (4, 410502120, 'Judah', 'Lowe', 'f', '2004-01-23', 'Ap #629-3863 Ullamcorper. Ave', 'Vancouver', 'WA', 938517480, 5846958210, 'Suspendisse.aliquet@ac.net', 'n', 'n', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (5, 316228567, 'Cassidy', 'Mullins', 'm', '2000-12-24', 'P.O. Box 676, 7244 In Av.', 'Cedar Rapids', 'IA', 306049577, 3327030237, 'netus@Morbi.com', 'n', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (6, 883116938, 'Ulla', 'Mayer', 'f', '2007-12-25', '250-8069 Laoreet, St.', 'Nampa', 'ID', 964887121, 7347404025, 'eleifend.Cras.sed@vel.net', 'n', 'y', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (7, 779200159, 'Vernon', 'Cooper', 'f', '2013-09-26', 'P.O. Box 884, 6356 Eu, Road', 'Des Moines', 'IA', 781605357, 3183719035, 'Vestibulum.ut.eros@Nullasemper.co.uk', 'y', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (8, 375753560, 'Burton', 'Welch', 'm', '1989-10-24', '248-1749 Sed Rd.', 'Joliet', 'IL', 821446583, 3973168759, 'tempor.arcu.Vestibulum@Aliquamvulputateullamcorper.com', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (9, 29616172, 'Stephen', 'Cote', 'm', '1992-05-17', 'Ap #812-3970 Nisl. Rd.', 'Annapolis', 'MD', 339384225, 2820031440, 'Lorem.ipsum@Vivamusmolestiedapibus.net', 'y', 'n', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (10, 407165001, 'Hunter', 'Roman', 'f', '1991-11-25', 'Ap #985-4799 Sit St.', 'Warren', 'MI', 803935024, 9648306833, 'Mauris@auctorquistristique.org', 'y', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (11, 674184256, 'Glenna', 'Barr', 'm', '1989-10-31', 'P.O. Box 439, 7577 Nunc Ave', 'Oklahoma City', 'OK', 489360000, 7965551747, 'Maecenas.ornare.egestas@mollisdui.com', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (12, 404488746, 'Emmanuel', 'Peters', 'm', '2019-05-12', '449-583 Nunc. Rd.', 'Denver', 'CO', 158487028, 7447833841, 'non@diamPellentesquehabitant.ca', 'y', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (13, 67064417, 'Alexa', 'Hood', 'f', '1988-02-29', 'P.O. Box 524, 8880 Non St.', 'Glendale', 'AZ', 429376580, 8360190759, 'nibh.Aliquam.ornare@aodiosemper.net', 'y', 'y', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (14, 706943622, 'Knox', 'Rocha', 'f', '2002-05-16', '3449 Pellentesque Rd.', 'Louisville', 'KY', 232579458, 5606064307, 'Pellentesque.habitant@congueaaliquet.net', 'n', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (15, 524029473, 'Zeph', 'Lopez', 'm', '1984-07-12', '430-9451 Tortor. Av.', 'Cincinnati', 'OH', 880723564, 7988342555, 'sed@Sed.co.uk', 'y', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (16, 601117200, 'Alyssa', 'Wiggins', 'm', '1996-11-19', 'P.O. Box 340, 9293 Eu Street', 'Des Moines', 'IA', 523611233, 8094290176, 'fermentum.risus.at@faucibusorci.ca', 'n', 'n', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (17, 83962989, 'Russell', 'Jackson', 'm', '1986-10-26', 'Ap #710-9994 Gravida. Rd.', 'Jonesboro', 'AR', 571474552, 5827001937, 'molestie.Sed@magnis.net', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (18, 779700995, 'Phoebe', 'Shaw', 'f', '1991-01-13', 'P.O. Box 217, 8646 Lacus, Av.', 'Burlington', 'VT', 639038716, 9300575682, 'a.felis.ullamcorper@nibh.com', 'y', 'n', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (19, 208770468, 'Wallace', 'Bruce', 'm', '1997-07-14', '409-3228 Mauris Street', 'Minneapolis', 'MN', 187469766, 2258246640, 'Duis.ac@habitantmorbitristique.co.uk', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (20, 96812766, 'Seth', 'Burton', 'm', '1997-01-11', '6355 Non, Ave', 'Paradise', 'NV', 129512999, 4252056868, 'non@convallis.co.uk', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (21, 465027747, 'Chloe', 'Mccall', 'f', '1987-06-20', 'P.O. Box 803, 9487 Velit Avenue', 'Columbus', 'OH', 473820832, 9452399578, 'ac.nulla.In@miDuis.co.uk', 'n', 'y', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (22, 729975518, 'Baxter', 'Morales', 'f', '2014-03-03', 'P.O. Box 610, 6431 Sed Av.', 'Grand Rapids', 'MI', 881536782, 6296834362, 'mollis.Duis.sit@sapienNuncpulvinar.com', 'y', 'n', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (23, 690703720, 'Cailin', 'Buckley', 'm', '1993-03-10', 'P.O. Box 965, 390 Eu Road', 'Frederick', 'MD', 146349362, 2150611984, 'enim.diam.vel@euismodacfermentum.net', 'n', 'n', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (24, 952128766, 'Destiny', 'Shaw', 'm', '2003-07-20', '6199 Vulputate Street', 'Missoula', 'MT', 855009424, 3378086926, 'enim@Sednec.co.uk', 'n', 'y', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (25, 719157938, 'Olympia', 'Frazier', 'f', '1992-06-05', 'Ap #503-2534 Dis Rd.', 'Knoxville', 'TN', 888243820, 6826093453, 'porttitor.scelerisque@sagittisNullam.org', 'y', 'y', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (26, 253525953, 'Maryam', 'Sloan', 'm', '2011-06-01', '134-525 A Road', 'Frederick', 'MD', 382369535, 3178007925, 'Praesent.eu.nulla@Integertinciduntaliquam.ca', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (27, 592338360, 'Leonard', 'Foreman', 'f', '2003-12-30', 'P.O. Box 781, 2506 Ac Rd.', 'Miami', 'FL', 931570547, 7638005995, 'id.magna.et@antebibendum.net', 'n', 'y', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (28, 169006971, 'Galvin', 'Lee', 'm', '2018-09-23', 'P.O. Box 110, 7663 Risus. Rd.', 'Chattanooga', 'TN', 948346845, 5183148375, 'elit@Namac.org', 'y', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (29, 49079344, 'Ursa', 'Peters', 'm', '2011-01-30', '840-2298 Ultricies Av.', 'Owensboro', 'KY', 643030824, 1365268375, 'scelerisque.lorem@Cumsociisnatoque.com', 'y', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (30, 848918914, 'Amal', 'Sparks', 'm', '1985-12-12', '4757 Nulla. St.', 'Lowell', 'MA', 126839978, 6840171292, 'enim.consequat.purus@eros.net', 'n', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (31, 953980000, 'Serena', 'Hodges', 'm', '1994-02-12', 'P.O. Box 270, 3252 Erat. Avenue', 'Metairie', 'LA', 222339610, 7226412817, 'dolor.Donec.fringilla@dolorvitaedolor.net', 'y', 'y', 'n', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (32, 279582512, 'Avye', 'Rivas', 'f', '1994-11-19', 'Ap #147-1616 Integer Av.', 'Hilo', 'HI', 429963077, 1468206701, 'odio.Aliquam.vulputate@commodoatlibero.edu', 'y', 'n', 'y', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (33, 546702735, 'Nevada', 'Armstrong', 'm', '2015-07-16', '9053 Quam St.', 'Lincoln', 'NE', 257034626, 3927638588, 'sem.egestas.blandit@libero.org', 'n', 'n', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (34, 552635408, 'Roth', 'Baxter', 'f', '1992-06-09', 'P.O. Box 283, 5849 Praesent St.', 'Oklahoma City', 'OK', 248411772, 5436352135, 'lectus.pede.et@porttitortellus.net', 'y', 'n', 'y', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (35, 265348179, 'Melvin', 'Marshall', 'f', '2012-03-20', 'P.O. Box 186, 8433 Sem Av.', 'Lexington', 'KY', 393881356, 7669216713, 'in.hendrerit.consectetuer@nuncullamcorpereu.co.uk', 'y', 'y', 'n', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (36, 778657337, 'Ishmael', 'Foreman', 'f', '2008-12-28', 'Ap #146-1606 Nibh Road', 'West Valley City', 'UT', 986516758, 4118597660, 'amet@auctor.org', 'y', 'n', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (37, 430096032, 'Amy', 'Burgess', 'f', '2008-12-17', 'Ap #926-5191 Volutpat. Rd.', 'Shreveport', 'LA', 717272578, 2406029014, 'velit@a.org', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (38, 467369662, 'Burke', 'Finley', 'f', '2001-08-22', '392-1970 Elementum, Rd.', 'Bellevue', 'WA', 920615814, 7364525182, 'dolor.quam.elementum@dui.org', 'y', 'n', 'y', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (39, 999339067, 'Oleg', 'Kirby', 'm', '1993-08-24', 'P.O. Box 517, 8760 Eros. Rd.', 'Kearney', 'NE', 962690027, 8942465943, 'sit.amet.massa@orciadipiscingnon.edu', 'y', 'n', 'n', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (40, 155646521, 'Lewis', 'Farmer', 'm', '1997-01-26', 'P.O. Box 141, 8245 Malesuada Rd.', 'North Las Vegas', 'NV', 290952540, 8396602335, 'orci.consectetuer@sitamet.net', 'n', 'y', 'y', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (41, 106340617, 'Haley', 'Luna', 'm', '2013-04-27', 'Ap #598-5111 Cras Rd.', 'Lewiston', 'ME', 120328633, 6037311512, 'augue.id.ante@maurisblanditmattis.co.uk', 'n', 'y', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (42, 460032664, 'Hannah', 'Fields', 'm', '2001-10-30', 'P.O. Box 351, 4059 Eget Road', 'San Antonio', 'TX', 518809515, 1828016957, 'vehicula.aliquet@necante.net', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (43, 861696300, 'Vivian', 'Mueller', 'm', '1986-11-22', 'Ap #873-2785 Sed St.', 'Bloomington', 'MN', 525905230, 1337322290, 'ullamcorper.viverra@duinec.co.uk', 'y', 'y', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (44, 644122630, 'Echo', 'Holmes', 'f', '1996-12-11', '544-6263 Consequat Av.', 'Savannah', 'GA', 514778917, 6158751083, 'hendrerit.consectetuer.cursus@enim.com', 'n', 'y', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (45, 439176538, 'Conan', 'Blackburn', 'm', '2004-12-08', '538-5270 Nunc. Rd.', 'Green Bay', 'WI', 701254025, 1193499970, 'Donec.feugiat@dolorsitamet.com', 'n', 'y', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (46, 16321305, 'Iola', 'Rivers', 'f', '1996-02-27', '487 Porttitor Av.', 'Eugene', 'OR', 475453738, 3025103103, 'amet.risus.Donec@per.net', 'y', 'y', 'n', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (47, 805452393, 'Kenneth', 'Lester', 'm', '2011-01-26', '4643 Est St.', 'Lexington', 'KY', 822981783, 1229982210, 'pellentesque@lorem.edu', 'n', 'y', 'n', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (48, 418664873, 'Ciaran', 'Hampton', 'f', '1981-07-09', '465-6397 Magna Rd.', 'Saint Louis', 'MO', 945927705, 6684060535, 'per.conubia.nostra@dolordolor.net', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (49, 917452401, 'Uriel', 'Jarvis', 'f', '2010-04-02', 'Ap #482-308 Tempor St.', 'Harrisburg', 'PA', 564296613, 3769626298, 'lacus.Ut@magnaPhasellus.com', 'y', 'y', 'n', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (50, 222565683, 'Yoko', 'Kidd', 'm', '1998-08-09', 'Ap #804-3873 Sodales Av.', 'Denver', 'CO', 963915094, 5628972483, 'in.dolor@nullaCraseu.com', 'n', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (51, 233395008, 'Lev', 'Bird', 'm', '1994-03-08', '485-877 Ut Rd.', 'Kaneohe', 'HI', 518447922, 2679982131, 'netus@Donecfeugiatmetus.co.uk', 'n', 'n', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (52, 926425125, 'Kerry', 'Scott', 'm', '2001-12-06', '550-2320 Pellentesque, St.', 'Allentown', 'PA', 953283277, 7157628031, 'nec.enim@sitamet.net', 'n', 'n', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (53, 51808217, 'Jonah', 'Dillon', 'f', '1993-05-05', 'Ap #536-1231 Rhoncus. Ave', 'Rockville', 'MD', 987196981, 3370459708, 'odio.Aliquam.vulputate@tempus.ca', 'y', 'y', 'y', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (54, 842829393, 'Bert', 'Gardner', 'm', '2009-08-06', 'Ap #168-8442 Dui, Rd.', 'Oklahoma City', 'OK', 563628823, 6016280315, 'Quisque.ac.libero@mollisPhasellus.org', 'y', 'n', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (55, 820086083, 'Kamal', 'Castillo', 'm', '2013-07-02', '9240 In Av.', 'Springfield', 'MA', 841929722, 9530565237, 'adipiscing@Donecconsectetuer.com', 'n', 'y', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (56, 198243825, 'McKenzie', 'Stanton', 'f', '2017-08-23', '986-4612 Mauris Avenue', 'Philadelphia', 'PA', 619773641, 3308502220, 'metus.Vivamus.euismod@erat.org', 'n', 'n', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (57, 443398302, 'Tucker', 'Hammond', 'm', '2002-12-10', 'Ap #730-2333 Iaculis, Avenue', 'Gresham', 'OR', 249673300, 8941714614, 'ligula.tortor.dictum@adipiscingMauris.co.uk', 'y', 'y', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (58, 855264172, 'Lucy', 'Navarro', 'f', '1987-02-18', '490-6460 Sit Av.', 'Reading', 'PA', 767357977, 7585294602, 'est@Nulla.net', 'y', 'y', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (59, 820716368, 'Demetrius', 'Giles', 'f', '1984-07-30', 'P.O. Box 748, 8518 At, St.', 'Honolulu', 'HI', 888579560, 9802656496, 'diam@Vivamusnibhdolor.co.uk', 'y', 'y', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (60, 37531691, 'Noel', 'Bailey', 'f', '2012-08-23', '736-7917 Luctus Road', 'Rutland', 'VT', 588203272, 2051932712, 'Maecenas.iaculis.aliquet@turpis.co.uk', 'y', 'y', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (61, 133907516, 'Nash', 'Stark', 'm', '2002-10-11', '6900 Id St.', 'Springdale', 'AR', 336241130, 5305635428, 'ultrices.Vivamus.rhoncus@pedesagittis.com', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (62, 605386162, 'Belle', 'Gross', 'm', '1993-07-28', '964-2663 Sed Road', 'Shreveport', 'LA', 581387363, 8901625027, 'magna.Duis.dignissim@duisemper.com', 'y', 'n', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (63, 669817040, 'Paula', 'Bird', 'm', '2001-04-21', 'P.O. Box 350, 9517 Et Rd.', 'Lexington', 'KY', 830508185, 7658212106, 'et.tristique.pellentesque@amet.org', 'y', 'n', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (64, 563813771, 'Hyatt', 'Boyle', 'm', '1998-05-24', '309-3698 Non Rd.', 'Cambridge', 'MA', 220040605, 1574035150, 'in@sedsemegestas.com', 'y', 'y', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (65, 570543049, 'Steven', 'Lowe', 'm', '2014-04-27', 'P.O. Box 630, 8498 Vel Street', 'Owensboro', 'KY', 235061877, 4913714532, 'nonummy.Fusce@turpisvitae.co.uk', 'y', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (66, 115446501, 'Kim', 'Meyer', 'f', '1993-05-01', '300-3772 Risus St.', 'Cleveland', 'OH', 583750818, 9813838038, 'iaculis.odio@ac.ca', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (67, 556314464, 'Jasmine', 'Wilson', 'm', '1982-04-22', '311-2608 Consequat Avenue', 'South Bend', 'IN', 128691722, 2864392068, 'sollicitudin.adipiscing.ligula@temporarcuVestibulum.org', 'y', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (68, 652222304, 'Raphael', 'Lane', 'f', '2004-04-11', '3921 Vel, Road', 'San Diego', 'CA', 215329341, 8857785761, 'velit.in.aliquet@fringilla.net', 'y', 'n', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (69, 568537994, 'Carla', 'Torres', 'f', '1987-04-09', 'Ap #958-7086 Cursus Ave', 'Southaven', 'MS', 288534592, 8687808451, 'ut@Aliquam.net', 'n', 'n', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (70, 788638264, 'Melissa', 'Jefferson', 'f', '1996-06-15', 'P.O. Box 291, 7008 Luctus Ave', 'Covington', 'KY', 334435081, 5084864691, 'arcu.Vestibulum@urnaet.edu', 'n', 'n', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (71, 326024616, 'Cailin', 'Hernandez', 'f', '2002-08-22', 'Ap #352-1580 Ligula. St.', 'Bloomington', 'MN', 456462837, 6253374177, 'ipsum.porta@volutpat.ca', 'n', 'y', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (72, 920907351, 'Katell', 'Mann', 'f', '1983-07-16', 'P.O. Box 488, 874 At St.', 'Ketchikan', 'AK', 580592173, 8670938884, 'justo.eu.arcu@perconubianostra.com', 'n', 'y', 'y', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (73, 60716197, 'Harriet', 'Brady', 'm', '2012-10-23', 'Ap #435-8298 Nullam Ave', 'Athens', 'GA', 444425710, 1162368009, 'ante@ProinvelitSed.ca', 'y', 'y', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (74, 138822997, 'Merritt', 'Barrett', 'f', '1992-02-23', 'P.O. Box 290, 7075 Aliquam Street', 'Atlanta', 'GA', 448224941, 8455601247, 'pede.Cum@Nullamfeugiat.co.uk', 'y', 'n', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (75, 674647904, 'Kuame', 'Lott', 'm', '2004-09-05', '7847 Pharetra St.', 'Athens', 'GA', 913448143, 6328960462, 'pede.blandit@molestieSedid.edu', 'y', 'y', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (76, 208099024, 'Hollee', 'Maxwell', 'f', '2010-01-10', '803-6663 Posuere Av.', 'West Valley City', 'UT', 667682060, 5368011842, 'diam@necmauris.net', 'n', 'n', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (77, 537781397, 'Cally', 'Anderson', 'm', '2018-04-23', 'P.O. Box 418, 5816 Lobortis Street', 'Tampa', 'FL', 286622491, 7482540735, 'molestie@maurissapien.org', 'y', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (78, 700064885, 'Fay', 'Hinton', 'm', '1995-08-14', '9727 Suscipit Ave', 'Tucson', 'AZ', 861922112, 3019408525, 'sed.facilisis.vitae@dolorNullasemper.org', 'y', 'n', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (79, 329967070, 'Sara', 'Fry', 'f', '2018-03-16', 'P.O. Box 300, 6390 Senectus Av.', 'Grand Rapids', 'MI', 350256219, 9343621304, 'eu@egestasblanditNam.net', 'y', 'n', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (80, 645975550, 'Vincent', 'Frye', 'm', '2003-01-29', 'P.O. Box 714, 7686 Hendrerit Road', 'West Valley City', 'UT', 551940582, 2282734529, 'pellentesque.eget.dictum@rhoncus.net', 'n', 'n', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (81, 502389726, 'Felix', 'Steele', 'm', '2018-06-22', 'Ap #540-2471 Curae; Street', 'Davenport', 'IA', 924095106, 9862438307, 'ac.metus.vitae@Aliquam.ca', 'y', 'n', 'n', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (82, 30450553, 'Lois', 'Rocha', 'f', '2004-05-12', 'Ap #215-2465 Vel Road', 'Saint Paul', 'MN', 879479157, 8304766221, 'eu.ultrices.sit@lacusvariuset.edu', 'n', 'y', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (83, 621210885, 'Castor', 'Strong', 'm', '2004-11-06', '526-941 Ac Rd.', 'Los Angeles', 'CA', 885387016, 6584027732, 'consectetuer.adipiscing@pedePraesent.org', 'y', 'y', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (84, 641542453, 'Ryan', 'Leach', 'm', '2010-01-12', 'P.O. Box 537, 6795 Commodo St.', 'Biloxi', 'MS', 647837350, 5724433908, 'arcu@cubiliaCuraeDonec.co.uk', 'y', 'y', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (85, 27696255, 'Solomon', 'Mcclure', 'f', '2007-12-13', 'Ap #544-9797 Eros Av.', 'Colorado Springs', 'CO', 720647720, 7390179038, 'in.consequat.enim@bibendumsedest.edu', 'n', 'y', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (86, 280074970, 'Amal', 'Cabrera', 'f', '2009-02-18', 'P.O. Box 826, 7865 Tempor Street', 'Annapolis', 'MD', 275276737, 2948865319, 'Donec@semmagna.org', 'n', 'y', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (87, 672897232, 'Luke', 'Dotson', 'f', '1993-12-13', 'P.O. Box 366, 2151 Fusce Rd.', 'Little Rock', 'AR', 528386800, 8024790452, 'non.magna@Aliquamrutrumlorem.org', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (88, 662293394, 'Dorian', 'Houston', 'f', '2016-01-18', 'P.O. Box 860, 2667 Duis Rd.', 'Athens', 'GA', 750583058, 5429714926, 'eu@tempus.co.uk', 'n', 'y', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (89, 465887356, 'Cade', 'Wallace', 'm', '2014-05-18', '3618 Lacus Rd.', 'Wyoming', 'WY', 454244206, 4391016310, 'erat.volutpat@euelit.com', 'y', 'n', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (90, 216929951, 'Silas', 'Bond', 'f', '1986-07-29', '969-9587 Sapien St.', 'Fort Worth', 'TX', 785642388, 8541030031, 'cursus.purus@famesac.ca', 'n', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (91, 695759377, 'Adara', 'Reese', 'm', '1992-08-17', 'Ap #974-5255 Mollis. Ave', 'Portland', 'OR', 579290987, 6589998491, 'amet.ante.Vivamus@tortordictum.net', 'y', 'y', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (92, 757042172, 'Isaiah', 'Ashley', 'f', '2008-06-20', 'Ap #905-3933 Libero. Av.', 'Fort Wayne', 'IN', 206977025, 3905834129, 'eu.elit.Nulla@sapien.com', 'y', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (93, 898048934, 'Marsden', 'Holt', 'f', '1999-02-07', '527-1915 A, Rd.', 'Huntsville', 'AL', 620795484, 5801399544, 'mauris.ut@non.edu', 'y', 'y', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (94, 665349732, 'Rahim', 'Kinney', 'm', '2011-01-13', 'P.O. Box 604, 4239 Dolor. Ave', 'South Burlington', 'VT', 112069143, 1422652126, 'eros.turpis.non@pellentesqueeget.net', 'y', 'n', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (95, 438323737, 'Galena', 'Guerra', 'f', '1989-02-15', 'P.O. Box 309, 9343 Elit Avenue', 'West Jordan', 'UT', 555729115, 9064674100, 'ornare.elit.elit@magna.com', 'n', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (96, 232025483, 'Maisie', 'Cooke', 'm', '2011-10-06', 'Ap #424-7086 In St.', 'Pike Creek', 'DE', 149147776, 6947002245, 'nulla.ante@lacusQuisque.net', 'y', 'n', 'y', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (97, 309181047, 'Ella', 'Cooke', 'm', '2014-03-30', 'Ap #678-9727 Metus St.', 'Covington', 'KY', 204851472, 4265292737, 'rutrum.eu@luctusfelis.net', 'y', 'n', 'n', 'Lorem');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (98, 574939057, 'Zelenia', 'Rocha', 'f', '2008-08-07', '201-5914 Enim St.', 'Reading', 'PA', 856063342, 3133656237, 'Proin.dolor.Nulla@egestasurna.org', 'n', 'n', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (99, 73917412, 'Kylynn', 'Dunn', 'f', '2010-07-31', 'Ap #970-6089 Mauris Street', 'Tacoma', 'WA', 419224391, 6249367520, 'euismod.ac@anteNuncmauris.co.uk', 'y', 'y', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (100, 390811509, 'Audra', 'Grant', 'm', '1999-02-27', 'P.O. Box 496, 4089 Dolor Road', 'Portland', 'OR', 250796293, 8992984609, 'Quisque@elit.co.uk', 'y', 'n', 'y', 'Lorem ipsum dolor sit');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mtd16`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `mtd16`;
INSERT INTO `mtd16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (1, 'Mrs.', $1,935.59 , 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (2, 'Mr.', $5,058.33 , 'y', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (3, 'Mrs.', $4,181.33 , 'n', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (4, 'Mr.', $9,790.62 , 'y', 'y', 'Lorem');
INSERT INTO `mtd16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (5, 'Mr.', $2,661.34 , 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (6, 'Mr.', $7,658.60 , 'n', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (7, 'Mr.', $7,946.21 , 'y', 'n', 'Lorem');
INSERT INTO `mtd16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (8, 'Mr.', $2,110.50 , 'n', 'y', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (9, 'Ms.', $2,898.88 , 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (10, 'Mrs.', $9,155.17 , 'y', 'y', 'Lorem ipsum dolor sit');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mtd16`.`faculty`
-- -----------------------------------------------------
START TRANSACTION;
USE `mtd16`;
INSERT INTO `mtd16`.`faculty` (`per_id`, `fac_rank`, `fac_start_date`, `fac_end_date`, `fac_notes`) VALUES (1, 'Associate Professor', '2013-04-11', '2009-11-09', 'vel turpis. Aliquam adipiscing lobortis');
INSERT INTO `mtd16`.`faculty` (`per_id`, `fac_rank`, `fac_start_date`, `fac_end_date`, `fac_notes`) VALUES (2, 'Professor', '2006-04-04', '1998-03-24', 'facilisis non, bibendum sed, est.');
INSERT INTO `mtd16`.`faculty` (`per_id`, `fac_rank`, `fac_start_date`, `fac_end_date`, `fac_notes`) VALUES (3, 'Professor', '2003-04-05', '1982-09-04', 'ipsum primis');
INSERT INTO `mtd16`.`faculty` (`per_id`, `fac_rank`, `fac_start_date`, `fac_end_date`, `fac_notes`) VALUES (4, 'Instructor', '1981-12-10', '1992-07-22', 'netus et malesuada fames ac');
INSERT INTO `mtd16`.`faculty` (`per_id`, `fac_rank`, `fac_start_date`, `fac_end_date`, `fac_notes`) VALUES (5, 'Assistant Professor', '2000-05-09', '1992-04-15', 'elit.');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mtd16`.`student`
-- -----------------------------------------------------
START TRANSACTION;
USE `mtd16`;
INSERT INTO `mtd16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (21, 'accounting', 'y', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (22, 'industrial design', 'n', 'n', 'Lorem');
INSERT INTO `mtd16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (23, 'marketing', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (24, 'chemistry', 'y', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (25, 'communication', 'n', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (26, 'marketing', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (27, 'education', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (28, 'communication', 'y', 'n', 'Lorem ipsum');
INSERT INTO `mtd16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (29, 'art history', 'y', 'y', 'Lorem ipsum');
INSERT INTO `mtd16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (30, 'chemistry', 'n', 'y', 'Lorem');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mtd16`.`undergrad`
-- -----------------------------------------------------
START TRANSACTION;
USE `mtd16`;
INSERT INTO `mtd16`.`undergrad` (`per_id`, `ugd_test`, `ugd_score`, `ugd_standing`, `ugd_start_date`, `ugd_end_date`, `ugd_notes`) VALUES (21, 'act', 251, 'jr', '2014-01-22', '2018-04-16', 'Curabitur dictum. Phasellus in');
INSERT INTO `mtd16`.`undergrad` (`per_id`, `ugd_test`, `ugd_score`, `ugd_standing`, `ugd_start_date`, `ugd_end_date`, `ugd_notes`) VALUES (22, 'sat', 1086, 'jr', '2013-12-27', '2017-09-29', 'ac orci. Ut semper pretium');
INSERT INTO `mtd16`.`undergrad` (`per_id`, `ugd_test`, `ugd_score`, `ugd_standing`, `ugd_start_date`, `ugd_end_date`, `ugd_notes`) VALUES (23, 'act', 2323, 'jr', '2013-08-16', '2017-11-07', 'sociis natoque');
INSERT INTO `mtd16`.`undergrad` (`per_id`, `ugd_test`, `ugd_score`, `ugd_standing`, `ugd_start_date`, `ugd_end_date`, `ugd_notes`) VALUES (24, 'sat', 786, 'so', '2015-02-17', '2019-03-22', 'aliquet molestie tellus. Aenean egestas');
INSERT INTO `mtd16`.`undergrad` (`per_id`, `ugd_test`, `ugd_score`, `ugd_standing`, `ugd_start_date`, `ugd_end_date`, `ugd_notes`) VALUES (25, 'act', 482, 'jr', '2016-10-12', '2018-10-05', 'tempor');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mtd16`.`grad`
-- -----------------------------------------------------
START TRANSACTION;
USE `mtd16`;
INSERT INTO `mtd16`.`grad` (`per_id`, `grd_test`, `grd_score`, `grd_start_date`, `grd_end_date`, `grd_notes`) VALUES (26, 'gmat', 1919, '2018-05-22', '2018-03-11', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`grad` (`per_id`, `grd_test`, `grd_score`, `grd_start_date`, `grd_end_date`, `grd_notes`) VALUES (27, 'lsat', 1635, '2018-01-31', '2018-09-01', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`grad` (`per_id`, `grd_test`, `grd_score`, `grd_start_date`, `grd_end_date`, `grd_notes`) VALUES (28, 'lsat', 1077, '2018-03-08', '2018-07-11', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`grad` (`per_id`, `grd_test`, `grd_score`, `grd_start_date`, `grd_end_date`, `grd_notes`) VALUES (29, 'lsat', 523, '2017-12-04', '2019-07-14', 'Lorem ipsum dolor sit');
INSERT INTO `mtd16`.`grad` (`per_id`, `grd_test`, `grd_score`, `grd_start_date`, `grd_end_date`, `grd_notes`) VALUES (30, 'gre', 1968, '2019-08-14', '2019-02-04', 'Lorem ipsum dolor sit amet,');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mtd16`.`staff`
-- -----------------------------------------------------
START TRANSACTION;
USE `mtd16`;
INSERT INTO `mtd16`.`staff` (`per_id`, `stf_position`, `stf_start_date`, `stf_end_date`, `stf_notes`) VALUES (6, 'Specialist', '2014-04-05', '2018-05-31', 'Sed nunc');
INSERT INTO `mtd16`.`staff` (`per_id`, `stf_position`, `stf_start_date`, `stf_end_date`, `stf_notes`) VALUES (7, 'Specialist', '2000-11-13', '2018-08-31', 'vel arcu eu');
INSERT INTO `mtd16`.`staff` (`per_id`, `stf_position`, `stf_start_date`, `stf_end_date`, `stf_notes`) VALUES (8, 'Associate Director', '2011-10-20', '2017-05-06', 'lorem semper auctor. Mauris vel');
INSERT INTO `mtd16`.`staff` (`per_id`, `stf_position`, `stf_start_date`, `stf_end_date`, `stf_notes`) VALUES (9, 'Career Liaison', '1983-07-19', '2017-04-11', 'purus. Duis elementum, dui quis');
INSERT INTO `mtd16`.`staff` (`per_id`, `stf_position`, `stf_start_date`, `stf_end_date`, `stf_notes`) VALUES (10, 'Communication', '1985-12-15', '2017-07-15', 'eleifend vitae, erat.');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mtd16`.`alumnus`
-- -----------------------------------------------------
START TRANSACTION;
USE `mtd16`;
INSERT INTO `mtd16`.`alumnus` (`per_id`, `alm_notes`) VALUES (11, '');
INSERT INTO `mtd16`.`alumnus` (`per_id`, `alm_notes`) VALUES (12, '');
INSERT INTO `mtd16`.`alumnus` (`per_id`, `alm_notes`) VALUES (13, '');
INSERT INTO `mtd16`.`alumnus` (`per_id`, `alm_notes`) VALUES (14, '');
INSERT INTO `mtd16`.`alumnus` (`per_id`, `alm_notes`) VALUES (15, '');
INSERT INTO `mtd16`.`alumnus` (`per_id`, `alm_notes`) VALUES (16, '');
INSERT INTO `mtd16`.`alumnus` (`per_id`, `alm_notes`) VALUES (17, '');
INSERT INTO `mtd16`.`alumnus` (`per_id`, `alm_notes`) VALUES (18, '');
INSERT INTO `mtd16`.`alumnus` (`per_id`, `alm_notes`) VALUES (19, '');
INSERT INTO `mtd16`.`alumnus` (`per_id`, `alm_notes`) VALUES (20, '');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mtd16`.`degree`
-- -----------------------------------------------------
START TRANSACTION;
USE `mtd16`;
INSERT INTO `mtd16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (11, 11, 'B.A.', 'Fine Arts', '1982-02-08', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (12, 12, 'A.S.', 'Science', '1930-03-06', 'Lorem ipsum dolor sit amet,');
INSERT INTO `mtd16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (13, 13, 'M.A.', 'Technology', '1998-01-25', 'Lorem');
INSERT INTO `mtd16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (14, 14, 'M.A.', 'Technology', '2023-01-04', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (15, 15, 'A.S.', 'Fine Arts', '1980-05-11', 'Lorem');
INSERT INTO `mtd16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (16, 16, 'B.S.', 'Business', '2013-10-31', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (17, 17, 'J.D.', 'Science', '1994-12-16', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (18, 18, 'M.F.A.', 'Engineering', '2010-06-04', 'Lorem ipsum dolor');
INSERT INTO `mtd16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (19, 19, 'M.A.', 'Business', '1985-08-31', 'Lorem ipsum');
INSERT INTO `mtd16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (20, 20, 'A.A.', 'Technology', '1982-05-06', 'Lorem ipsum dolor sit');

COMMIT;

